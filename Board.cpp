#include "Board.h"

std::optional<Piece>& Board::operator[](const Position& position)
{
	const auto& [rowIndex, columnIndex] = position;
	if (rowIndex < m_lenght && columnIndex < m_width) {
		return m_board[rowIndex * m_lenght + columnIndex];
	}
	throw "Index out of range";
}

const std::optional<Piece>& Board::operator[](const Position& position) const
{
	const auto& [rowIndex, columnIndex] = position;
	if (rowIndex < m_lenght && columnIndex < m_width) {
		return m_board[rowIndex * m_lenght + columnIndex];
	}
	throw "Index out of range";
}
//cel fara const va fi folosit << si cel cu const >>
const char emptyBoardCell[] = "_____";
std::ostream& operator<<(std::ostream& out, const Board& board)
{
	Board::Position position;
	auto& [row, column] = position;
	for (row = 0; row < Board::m_lenght; row++)
	{
		for (column = 0; column < Board::m_width; column++)
		{
			if (auto& optPiece=board[position]; optPiece) {
				out << optPiece.value() << " ";
			}
			else {
				out << emptyBoardCell << " ";
			}
		}
		out << "\n";
	}
	return out;
}
