#include "UnusedPieces.h"
#include<sstream>
std::ostream& operator<<(std::ostream& stream, const UnusedPieces& other)
{
	/*for (const auto& pair : other.m_pool) {
		stream << pair.first << " ";
	}*/



	for (const auto& [piece_name, piece] : other.m_pool) {
		stream << piece_name << " ";
	}

	return stream;
}

UnusedPieces::UnusedPieces()
{
	
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::SHORT, Piece::Shape::SQUARE, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::SHORT, Piece::Shape::SQUARE, Piece::Body::HOLLOW });
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::SHORT, Piece::Shape::ROUND, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::SHORT, Piece::Shape::ROUND, Piece::Body::HOLLOW });
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::TALL, Piece::Shape::SQUARE, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::TALL, Piece::Shape::SQUARE, Piece::Body::HOLLOW });
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::TALL, Piece::Shape::ROUND, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::LIGHT, Piece::Height::TALL, Piece::Shape::ROUND, Piece::Body::HOLLOW });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::SHORT, Piece::Shape::SQUARE, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::SHORT, Piece::Shape::SQUARE, Piece::Body::HOLLOW });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::SHORT, Piece::Shape::ROUND, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::SHORT, Piece::Shape::ROUND, Piece::Body::HOLLOW });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::TALL, Piece::Shape::SQUARE, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::TALL, Piece::Shape::SQUARE, Piece::Body::HOLLOW });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::TALL, Piece::Shape::ROUND, Piece::Body::FULL });
	EmplacePiece(Piece{ Piece::Color::DARK, Piece::Height::TALL, Piece::Shape::ROUND, Piece::Body::HOLLOW });
	


}

Piece UnusedPieces::PickPiece(const std::string& name)
{
	if (auto nodeHandler = m_pool.extract(name); nodeHandler) {
		return std::move(nodeHandler.mapped());
	}
	
	//auto mapIterator=m_pool.find(name);
	//if (mapIterator != m_pool.end()) {
	//	Piece tempPiece = std::move(mapIterator->second);
	//	m_pool.erase(mapIterator);
	//	return tempPiece;
	//	//copy elision - c++ 17
	//}
	else {
		throw "Piece not found!";
	}
	
}

void UnusedPieces::EmplacePiece(Piece&& piece)
{

	std::stringstream namePieceStream;
	namePieceStream << piece;
	//m_pool.emplace(namePieceStream.str(), piece, std::forward<Piece&&>(piece));
	m_pool.insert(std::make_pair(namePieceStream.str(), std::forward<Piece&&>(piece)));



}

