#include<iostream>
#include"Piece.h"
#include"Board.h"
#include"UnusedPieces.h"
#include"Piece.h"
int main()
{
    Piece piece( Piece::Color::DARK, Piece::Height::SHORT, Piece::Shape::SQUARE, Piece::Body::FULL);
    std::cout << "Full, Dark, Short, Square piece: " << piece << std::endl;

    Board board;
    std::cout << "Empty board:\n" << board << std::endl;

    board[{0, 0}] = std::move(piece);
    std::cout << "Moved piece to board:\n" << board << std::endl;

    UnusedPieces unusedPieces;
    std::cout << "All available pieces:\n" << unusedPieces << std::endl;
    unusedPieces.PickPiece("0001");
    std::cout << "Extracted \"0001\" remaining pieces after extracted:\n" << unusedPieces << std::endl;
	
	system("pause");

}