#include "Player.h"

Player::Player(const std::string& name): m_name(name)
{
}

Piece Player::pickPiece(std::istream& ist, UnusedPieces& unusedPieces) const
{
    std::string name;
    ist >> name;

    return unusedPieces.PickPiece(name);

}
//
//Board::Position Player::placePiece(std::istream& ist, Piece&& piece, Board& board) const {
//    Board::Position position;
//    uint16_t rowIndex= UINT16_MAX, columnIndex= UINT16_MAX;
//   // auto& [rowIndex, columnIndex] = position;
//    if (ist >> rowIndex)
//       if( ist >> columnIndex) {
//        Board::Position position = std::make_pair(static_cast<uint8_t>(rowIndex), static_cast<uint8_t>(columnIndex));
//        auto& optionalPiece = board[position];
//        if (optionalPiece.has_value()) {
//            throw "There is already one piece on this position";
//        }
//        optionalPiece = std::move(piece); 
//        return position;
//    }
//    ist.clear();
//    ist.seekg(std::ios::end);
//    throw "Please enter numbers between 0 and 3";
//}



std::ostream& operator<<(std::ostream& ost, const Player& player)
{
    return ost << player.m_name;
}
