#pragma once
#include<array>
#include<optional>
#include"Piece.h"
class Board
{
public:
	//Board() = default;
	using Position = std::pair<uint8_t, uint8_t>;

	std::optional<Piece>& operator[](const Position& position);
	const std::optional<Piece>& operator[](const Position& position) const;

	friend std::ostream& operator<<(std::ostream& out, const Board& board);
	//afisare
private: 
	static const int m_width = 4;
	static const int m_lenght = 4;
	static const int m_size = m_lenght * m_width;
	std::array<std::optional<Piece>, m_size> m_board;

};
