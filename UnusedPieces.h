#pragma once
#include"Piece.h"
#include<unordered_map>

class UnusedPieces
{
public:
	UnusedPieces();
	friend std::ostream& operator<<(std::ostream& stream, const UnusedPieces& other);
	Piece PickPiece(const std::string& name);
private:
	std::unordered_map<std::string, Piece> m_pool;
	void EmplacePiece(Piece&& piece);



};

