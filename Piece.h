#pragma once
#include<cstdint>
#include<fstream>
class Piece {
	public: 
	enum class  Color : uint8_t {
		    NONE,
			LIGHT,
			DARK 
			
	};
	enum class Height : uint8_t {
		NONE,
		SHORT,
		TALL 
		
	};
	enum class Shape : uint8_t {
		NONE,
		ROUND,
		SQUARE
	};
	enum class Body: uint8_t {
		NONE,
		HOLLOW,
		FULL
	};
	

public: 
	Piece(Color color, Height height, Shape shape, Body body);
	Piece();
	~Piece();
	Piece(const Piece& other);
	Piece(Piece&& other);
	Piece& operator=(const Piece& other);
	Piece& operator=(Piece&& other);
	Color GetColor() const noexcept;
	Height GetHeight() const noexcept;
	Shape GetShape()const noexcept;
	Body GetBody()const noexcept;
	friend std::ostream& operator<<(std::ostream& stream, const Piece& other);

private:
	Color m_color : 2;
	Height m_height : 2;
	Shape m_shape : 2;
	Body m_body : 2;
	
};
