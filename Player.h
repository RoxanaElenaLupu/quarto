#pragma once
#include<string>
#include"UnusedPieces.h"
#include"Piece.h"
#include"Board.h"
class Player
{
public:
	Player(const std::string& name);
	Piece pickPiece(std::istream& ist, UnusedPieces& unusedPieces) const;
	friend std::ostream& operator<<(std::ostream& ost, const Player& player);
	//Board::Position placePiece(std::istream& ist, Piece&& piece, Board& board) const;
private:
	std::string m_name;
};

