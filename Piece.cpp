#include "Piece.h"
Piece::Piece()
    :Piece(Piece::Color::NONE, Piece::Height::NONE, Piece::Shape::NONE, Piece::Body::NONE)
{

}

Piece::Piece(Color color, Height height, Shape shape, Body body)
    : m_color{ color }, m_height{ height }, m_shape{ shape }, m_body{body}
{
    //verificare la compilare
    static_assert(sizeof(*this) == 1, "Size greater tahn 1");
   
}



Piece::~Piece()
{
    m_body = Piece::Body::NONE;
    m_color = Piece::Color::NONE;
    m_height = Piece::Height::NONE;
    m_shape = Piece::Shape::NONE;
    
}

Piece::Piece(const Piece& other) {
    *this = other;
}

Piece::Piece(Piece&& other)
{
    *this = std::move(other);
}

Piece& Piece::operator=(const Piece& other)
{
    this->m_color = other.m_color;
    this->m_body = other.m_body;
    this->m_height = other.m_height;
    this->m_shape = other.m_shape;
    return *this;
}

Piece& Piece::operator=(Piece&& other)
{
    this->m_color = other.m_color;
    this->m_body = other.m_body;
    this->m_height = other.m_height;
    this->m_shape = other.m_shape;
    new (&other) Piece;
   /* other.m_body = Piece::Body::NONE;
    other.m_color = Piece::Color::NONE;
    other.m_height = Piece::Height::NONE;
    other.m_shape = Piece::Shape::NONE;*/

    return *this;
}

Piece::Color Piece::GetColor() const noexcept
{
    return m_color;
}

Piece::Height Piece::GetHeight() const noexcept
{
    return m_height;
}

Piece::Shape Piece::GetShape() const noexcept
{
    return m_shape;
}

Piece::Body Piece::GetBody() const noexcept
{
    return m_body;
}

std::ostream& operator<<(std::ostream& stream, const Piece& other)
{
    stream << static_cast<int>(other.m_body) - 1 
        << static_cast<int>(other.m_color) - 1
        << static_cast<int>(other.m_height) - 1
        << static_cast<int>(other.m_shape) - 1;

    return stream;
}
